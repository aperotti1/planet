AVILUG,ical,https://calendar.google.com/calendar/ical/hrffafrk10to2mb79qcka8rpo0%40group.calendar.google.com/public/basic.ics,Vicenza,https://www.avilug.it/
BgLUG Bergamo,ical,https://bglug.it/events/elenco/?ical,Bergamo,https://bglug.it
Bologna JS,meetup,https://www.meetup.com/it-IT/Bologna-JS-Meetup/,Bologna
Developers Italia,ical,https://mobilizon.it/@developers_ita/feed/ics,Developers Italia,https://developers.italia.it/
Free Circle,ical,https://www.thefreecircle.org/calendar/calendar.ics, Palermo,https://www.thefreecircle.org/
GoLang Milano,meetup,https://www.meetup.com/it-IT/Golang-Milano/,Milano
GOLEM,caldav,https://cloud.golem.linux.it/remote.php/dav/public-calendars/BWZ3N4Q0HRQN51YX,Firenze,https://golem.linux.it/
GRUSP,ical,https://calendar.google.com/calendar/ical/c_gsm3bhg1mgco1sskftfukd51f4%40group.calendar.google.com/public/basic.ics,GRUSP,https://www.grusp.org/
Hacklab Cormano,ical,https://cloud.hacklabcormano.it/remote.php/dav/public-calendars/zyGMGzkPFPxTj5fR/?export,Cormano,https://hacklabcormano.it/
Hacklab Cosenza,ical,https://calendar.google.com/calendar/ical/2705d768babd59b5e083958ec67da2329a581d3ab2623bc36e3c7a0e78893472%40group.calendar.google.com/public/basic.ics,Rende,https://hlcs.it
Haskell Milano,meetup,https://www.meetup.com/it-IT/haskell-milano/,Milano
ILS Firenze,ical,http://framagenda.org/remote.php/dav/public-calendars/HyrL4na9adxEf5jt?export,Firenze
ILS Perugia,ical,https://perugia.ils.org/events.ics,Perugia,https://perugia.ils.org/
ILS Rieti,ical,https://rieti.ils.org/events.ics,Rieti,https://rieti.ils.org/
ILS San Dona' di Piave,ical,https://calendar.google.com/calendar/ical/eg5goh3omajmv2dkvqtkaa1vgk%40group.calendar.google.com/public/basic.ics,San Dona' di Piave,https://www.ils.org/sezionilocali/sandonadipiave/
ILS Torino,ical,https://torino.ils.org/events.ics,Torino,https://torino.ils.org/
JUG Torino,meetup,https://www.meetup.com/it-IT/JUGTorino/,Torino
Legal Hackers Milano,meetup,https://www.meetup.com/it-IT/Legal-Hackers-Milano/,Milano
Legal Hackers Padova,meetup,https://www.meetup.com/it-IT/Legal-Hackers-Padova/,Padova
Legal Hackers Roma,meetup,https://www.meetup.com/it-IT/Legal-Hackers-Roma/,Roma
LugAnegA,ical,https://calendar.google.com/calendar/ical/nspgaa6qj4ub8dbf8cjeuuvpvg%40group.calendar.google.com/public/basic.ics,Belluno,https://www.luganega.org/
LUGMan,ical,https://owncloud.interdet-lsp.org/remote.php/dav/public-calendars/KGSGA1RD1ENYKQRX?export,Mantova,https://lugman.org
Lugotto,ical,https://lugotto.it/cloud/remote.php/dav/public-calendars/ZXFMay7DZWg8FZDz/?export,Brescia,https://lugotto.it/
Merge-IT,ical,https://codeat.owncube.com/remote.php/dav/public-calendars/MMsL6wAzAbgq9o44?export,Merge IT,https://online.merge-it.net/
Mittelab,ical,https://calendar.google.com/calendar/ical/70birgsfioo008s0hqridr6t0c%40group.calendar.google.com/public/basic.ics,Trieste,https://www.mittelab.org/
Mozilla Italia,ical,https://community.mozilla.org/events.ics?group=mozilla-italia, Mozilla Italia,https://www.mozillaitalia.org/
NaLUG,ical,https://mobilizon.it/events/going/QWwZLnDT3KFMqrPdMg55QY/ics,Napoli,https://www.nalug.tech/
OpenIT Este,ical,https://share.mailbox.org/ajax/share/0baf84f90c46e84ebe9c8c1c46e84ace8abc1c353555ae64/1/2/Y2FsOi8vMC80Mw,Padova,https://este.linux.it
Open Source Saturday Milano,meetup,https://www.meetup.com/it-IT/Open-Source-Saturday-Milano/,Milano
PNLUG,ical,https://share.pnlug.it/remote.php/dav/public-calendars/Z4TanyNxZTKffwnA/?export,Pordenone,https://www.pnlug.it/
Privacy Pride,ical,https://mobilizon.it/@privacypride/feed/ics,Italia,https://privacypride.org/
PUG Bologna,meetup,https://www.meetup.com/it-IT/pugbo-grusp/,Bologna
Pug Milano,meetup,https://www.meetup.com/it-IT/MilanoPHP/,Milano
PUG Modena,meetup,https://www.meetup.com/it-IT/pugmore/,Modena
PUG Romagna,meetup,https://www.meetup.com/it-IT/PUG-Romagna-PHP-User-Group-Romagnolo/,Modena
PUG Roma,meetup,https://www.meetup.com/it-IT/PUG-Roma/,Roma
PUG Seregno,meetup,https://www.meetup.com/it-IT/PUG-PHP-Seregno/,Seregno
PUG Sondrio,meetup,https://www.meetup.com/it-IT/pugSondrio-PHP-User-Group-Sondrio/,Sondrio
PUG Torino,meetup,https://www.meetup.com/it-IT/pugTO-PHP-User-Group-Torino/,Torino
PyData Venezia,meetup,https://www.meetup.com/it-IT/PyData-Venice/,Venezia
ReactJS Milano,meetup,https://www.meetup.com/it-IT/React-JS-Milano/,Milano
Roma Apache Spark,meetup,https://www.meetup.com/it-IT/Roma-Apache-Spark-Meetup/,Roma
RomaJS,meetup,https://www.meetup.com/it-IT/RomaJS/,Roma
Rust Milano,meetup,https://www.meetup.com/it-IT/rust-language-milano/,Milano
Rust Roma,meetup,https://www.meetup.com/it-IT/Rust-Roma/,Roma
Unplug,ical,https://mobilizon.it/events/going/TmjJX7we6be4bmVPFm5BK8/ics,Bari
Varie,ical,https://calendar.google.com/calendar/ical/ogrmnqujpl22edinh5c3k5rp74%40group.calendar.google.com/public/basic.ics,Planet
VeLug,ical,https://calendar.google.com/calendar/ical/jpigfmaa523b1qiqbngn5r0c94@group.calendar.google.com/public/basic.ics,Venezia
Woocommerce Torino,meetup,https://www.meetup.com/it-IT/Torino-WooCommerce-Meetup/,Torino
WordPress Ancona,meetup,https://www.meetup.com/it-IT/Meetup-WordPress-Ancona/,Ancona
WordPress Arezzo,meetup,https://www.meetup.com/it-IT/Arezzo-WordPress-Meetup/,Arezzo
WordPress Bari,meetup,https://www.meetup.com/it-IT/WordPress-Bari/,Bari
WordPress Bologna,meetup,https://www.meetup.com/it-IT/WordPress-Meetup-Bologna/,Bologna
WordPress Bolzano,meetup,https://www.meetup.com/it-IT/Bolzano-WordPress-Meetup/,Bolzano
WordPress Brescia,meetup,https://www.meetup.com/it-IT/WordPress-Meetup-Brescia/,Brescia
WordPress Catania,meetup,https://www.meetup.com/it-IT/Meetup-WordPress-Catania/,Catania
WordPress Cesena,meetup,https://www.meetup.com/it-IT/WordPress-Meetup-Cesena/,Cesena
WordPress Firenze,meetup,https://www.meetup.com/it-IT/WordPress-Meetup-Firenze/,Firenze
WordPress Genova,meetup,https://www.meetup.com/it-IT/WordPress-Meetup-Genova/,Genova
WordPress Imperia,meetup,https://www.meetup.com/it-IT/Imperia-WordPress-Meetup/,Imperia
WordPress Lecce,meetup,https://www.meetup.com/it-IT/WordPress-Meetup-Lecce/,Lecce
WordPress Lugano,meetup,https://www.meetup.com/it-IT/Lugano-WordPress-Meetup/,Lugano
WordPress Milano,meetup,https://www.meetup.com/it-IT/WordPress-Meetup-Milano/,Milano
WordPress Napoli,meetup,https://www.meetup.com/it-IT/WordPress-Napoli-Meetup/,Napoli
WordPress Novara,meetup,https://www.meetup.com/it-IT/WordPress-Meetup-Novara/,Novara
WordPress Padova,meetup,https://www.meetup.com/it-IT/Padova-WordPress-Meetup/,Padova
WordPress Palermo,meetup,https://www.meetup.com/it-IT/Palermo-WordPress-Meetup/,Palermo
WordPress Parma,meetup,https://www.meetup.com/it-IT/WordPress-Meetup-Parma/,Parma
WordPress Pavia,meetup,https://www.meetup.com/it-IT/Pavia-WordPress-Meetup/,Pavia
WordPress Piacenza,meetup,https://www.meetup.com/it-IT/Piacenza-WordPress-Meetup/,Piacenza
WordPress Ragusa,meetup,https://www.meetup.com/it-IT/wordpress-meetup-ragusa/,Ragusa
WordPress Reggio Emilia,meetup,https://www.meetup.com/it-IT/Reggio-Emilia-WordPress-Meetup/,Piacenza
WordPress Roma,meetup,https://www.meetup.com/it-IT/RomaWordPress/,Roma
WordPress Salerno,meetup,https://www.meetup.com/it-IT/Salerno-WordPress-Meetup/,Salerno
WordPress Teramo,meetup,https://www.meetup.com/it-IT/WordPress-Meetup-Teramo/,Teramo,https://wpteramo.it/
WordPress Terni,meetup,https://www.meetup.com/it-IT/WordPress-Meetup-Terni/,Terni,https://wpterni.it/
WordPress Torino,meetup,https://www.meetup.com/it-IT/WordPress-Meetup-Torino/,Torino,https://www.wptorino.it/
WordPress Treviso,meetup,https://www.meetup.com/it-IT/Treviso-WordPress-Meetup/,Treviso
WordPress Udine,meetup,https://www.meetup.com/it-IT/Udine-WordPress-Meetup/,Udine
WordPress Varese,meetup,https://www.meetup.com/it-IT/WordPress-Meetup-Varese/,Varese
WordPress Verona,meetup,https://www.meetup.com/it-IT/Verona-WordPress-Meetup/,Verona
WordPress Vicenza,meetup,https://www.meetup.com/it-IT/WordPress-Meetup-Vicenza/,Vicenza
